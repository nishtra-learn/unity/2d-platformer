﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator animator;
    private Rigidbody2D rb;
    private PlayerController playerController;

    void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        rb = GetComponentInChildren<Rigidbody2D>();
        playerController = GetComponent<PlayerController>();
    }

    
    void Update()
    {
        var horizontal_velocity = Mathf.Abs(rb.velocity.x);
        if (Mathf.Approximately(playerController.MoveDirection, 0))
            horizontal_velocity = 0;
        var vertical_velocity = rb.velocity.y;
        var isGrounded = playerController.IsGrounded();

        animator.SetFloat("horizontal_velocity", horizontal_velocity);
        animator.SetFloat("vertical_velocity", vertical_velocity);
        animator.SetBool("isGrounded", isGrounded);
    }
}
