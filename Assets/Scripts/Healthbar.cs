﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Slider slider;
    public Player player;


    // Start is called before the first frame update
    void Start()
    {
        slider.maxValue = player.HP;
        slider.value = player.HP;
        player.HpChange += (hp) => slider.value = hp;
    }

    
}
