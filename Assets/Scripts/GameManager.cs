﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text scoreText;
    public Transform playerSpawn;
    

    public int Score { get; private set; } = 0;

    public void IncreaseScore(int value)
    {
        Score += value;
        scoreText.text = $"Score: {Score}";
    }

    #region Singleton setup
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #endregion
}
