﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public Vector3 rotationAxis = new Vector3(0, 0, 1);
    [Tooltip("Degrees per second")]
    public float speed = 45f;
    public bool global = false;
    

    void Update()
    {
        var rotation = rotationAxis * speed;
        if (global) {
            transform.Rotate(rotation * Time.deltaTime, Space.World);
        }
        else {
            transform.Rotate(rotation * Time.deltaTime, Space.Self);
        }
    }
}
