﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject originator;
    public Vector2 moveDirection;
    public float moveSpeed = 5f;
    public float damage = 1f;

    private Rigidbody2D rb;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (moveDirection.x < 0) {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        Destroy(gameObject, 4);
    }

    
    private void FixedUpdate()
    {
        var position = rb.position + (moveDirection.normalized * moveSpeed * Time.fixedDeltaTime);
        rb.MovePosition(position);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        var damageableObj = other.gameObject.GetComponentInChildren<IDamageable>();
        bool isOriginator = originator.GetComponents<Collider2D>().Any(t => t == other);
        if (damageableObj != null && !isOriginator) {
            damageableObj.TakeDamage(damage);
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }
}
