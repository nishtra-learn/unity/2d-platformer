﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    public float hp = 5;
    public float moveSpeed = 5;
    public bool isStationary = true;
    public float aggroDistance = 10;
    public float rechargeTime = 2f;
    public float staySecondsAfterDeath = 2;
    public GameObject projectilePrefab;
    public Transform projectileSpawnPosition;

    private Rigidbody2D rb;
    private CircleCollider2D feetCollider;
    private Player target;
    private Animator animator;
    private bool isRecharging = false;
    private bool isAlive = true;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        feetCollider = GetComponent<CircleCollider2D>();
        target = GameObject.FindObjectOfType<Player>();
        animator = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        var vectorToTarget = target.transform.position - transform.position;
        if (isStationary)
        {
            if (vectorToTarget.magnitude < aggroDistance)
            {
                FaceTarget();
                Shoot();
            }
        }
        else
        {
            if (projectilePrefab != null)
            {
                LayerMask mask = LayerMask.GetMask("Player");
                var hit = Physics2D.Raycast(transform.position, new Vector2(transform.localScale.x, 0), aggroDistance, mask);
                if (hit.collider != null && hit.collider.GetComponentInParent<Player>() != null)
                {
                    Shoot();
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (!isStationary)
        {
            Move();
        }
    }

    private void Move()
    {
        if (!isAlive)
            return;

        var pos = transform.position;
        pos.x += moveSpeed * transform.localScale.x * Time.fixedDeltaTime;
        rb.MovePosition(pos);
        var hits = new List<RaycastHit2D>();
        var castDirection = new Vector2(transform.localScale.x, -0.2f).normalized;
        var filter = new ContactFilter2D().NoFilter();
        feetCollider.Cast(castDirection, filter, hits, feetCollider.bounds.size.x);
        if (hits.Count == 0)
        {
            var flippedScale = transform.localScale;
            flippedScale.x *= -1;
            transform.localScale = flippedScale;
        }
    }

    private void Shoot()
    {
        if (!isAlive)
            return;

        if (!isRecharging && projectilePrefab != null)
        {
            var projectileObj = Instantiate(projectilePrefab, projectileSpawnPosition.position, Quaternion.identity);
            var projectile = projectileObj.GetComponent<Projectile>();
            projectile.moveDirection = new Vector2(transform.localScale.x, 0);
            projectile.originator = gameObject;
            StartCoroutine(Recharge());
        }
    }

    IEnumerator Recharge()
    {
        isRecharging = true;
        yield return new WaitForSecondsRealtime(rechargeTime);
        isRecharging = false;
    }

    private void FaceTarget()
    {
        if (!isAlive)
            return;
            
        var vectorToTarget = target.transform.position - transform.position;
        if (vectorToTarget.x < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void TakeDamage(float damage)
    {
        hp -= damage;
        if (hp <= 0)
            Die();
    }

    private void Die()
    {
        isAlive = false;

        // disable all colliders
        var colliders = GetComponentsInChildren<Collider2D>();
        foreach (var item in colliders)
        {
            item.enabled = false;
        }
        rb.isKinematic = true;

        animator.SetBool("isDying", true);
                
        GameManager.Instance.IncreaseScore(10);
        //gameObject.SetActive(false);
        Destroy(gameObject, staySecondsAfterDeath);
    }
}
