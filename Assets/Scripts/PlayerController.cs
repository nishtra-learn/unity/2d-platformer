﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5;
    public float jumpForce = 5;
    public GameObject projectilePrefab;
    public Transform projectileSpawnPosition;

    private Rigidbody2D rb;
    private CircleCollider2D feetCollider;

    // input transfer
    public float MoveDirection { get; private set; }
    private bool pressedJump;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        feetCollider = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        MoveDirection = Input.GetAxisRaw("Horizontal");
        if (Input.GetKeyDown(KeyCode.Space))
            pressedJump = true;
        if (Input.GetKeyDown(KeyCode.Mouse0))
            Shoot();
    }

    private void Shoot()
    {
        var fireballObj = Instantiate(projectilePrefab, projectileSpawnPosition.position, Quaternion.identity);
        var fireballProjectile = fireballObj.GetComponentInChildren<Projectile>();
        fireballProjectile.originator = gameObject;
        fireballProjectile.moveDirection = new Vector2(transform.localScale.x, 0);
    }

    private void FixedUpdate()
    {
        if (MoveDirection != 0) Move(MoveDirection);
        if (pressedJump) Jump();
        if (rb.position.y < -6)
        {
            rb.Sleep();
            rb.position = GameManager.Instance.playerSpawn.position;
            rb.WakeUp();
        }
    }

    private void Jump()
    {
        pressedJump = false;
        if (IsGrounded())
        {
            var velocity = rb.velocity;
            velocity.y = jumpForce;
            rb.velocity = velocity;
        }
    }

    private void Move(float moveDirection)
    {
        var velocity = rb.velocity;
        velocity.x = speed * moveDirection;
        rb.velocity = velocity;
        if (moveDirection > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (moveDirection < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    public bool IsGrounded()
    {
        var colliders = new List<Collider2D>();
        feetCollider.OverlapCollider(new ContactFilter2D().NoFilter(), colliders);
        return colliders.Count > 1;
    }
}
