﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamageable
{
    [SerializeField]
    private float hp = 10;
    public float HP {
        get => hp;
        set {
            hp = value;
            HpChange?.Invoke(HP);
        }
    }
    public event Action<float> HpChange;


    public void TakeDamage(float damage)
    {
        HP -= damage;
        if (HP <= 0)
            Die();
    }

    private void Die()
    {
        var rb = gameObject.GetComponent<Rigidbody2D>();
        rb.Sleep();
        rb.position = GameManager.Instance.playerSpawn.position;
        rb.WakeUp();
        HP = 10;
    }
}
