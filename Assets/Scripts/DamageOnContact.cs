﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{
    public float damage = 1f;

    private void OnCollisionEnter2D(Collision2D other) {
        Player player = other.collider.GetComponentInParent<Player>();
        if (player != null) {
            player.TakeDamage(damage);
        }   
    }
}
